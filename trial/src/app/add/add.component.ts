import {
  Component,
  OnInit,
  ElementRef,
  ViewChild,
  Inject,
} from '@angular/core';
import { AsyncPipe, NgIf, DOCUMENT } from '@angular/common';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatSelectModule } from '@angular/material/select';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { provideNativeDateAdapter } from '@angular/material/core';
import { Router } from '@angular/router';

import {
  FormControl,
  FormGroup,
  Validators,
  FormsModule,
  ReactiveFormsModule,
} from '@angular/forms';

interface Status {
  label: string;
  value: string;
}

@Component({
  selector: 'app-add',
  standalone: true,
  providers: [provideNativeDateAdapter()],
  imports: [
    MatCardModule,
    MatButtonModule,
    NgIf,
    MatInputModule,
    MatFormFieldModule,
    MatDatepickerModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatAutocompleteModule,
    AsyncPipe,
  ],
  templateUrl: './add.component.html',
  styleUrl: './add.component.css',
  host: { ngSkipHydration: 'true' },
})
export class AddComponent implements OnInit {
  addForm: FormGroup;
  @ViewChild('input') input: ElementRef<HTMLInputElement>;
  statuses: Status[] = [
    { label: 'INACTIVE', value: 'inactive' },
    { label: 'BUSY', value: 'busy' },
    { label: 'ACTIVE', value: 'active' },
  ];
  options: string[] = [
    'Biru',
    'Hitam',
    'Hijau',
    'Kuning',
    'Orange',
    'Nila',
    'Merah',
    'Ungu',
    'Coklat',
    'Putih',
  ];
  filteredOptions: string[];
  currentData : any;
  constructor(
    private router: Router,
    @Inject(DOCUMENT) private document: Document
  ) {
    const localStorage = document.defaultView?.localStorage;

    if (localStorage) {
      let data: any = localStorage.getItem('employeesData');
      this.currentData = JSON.parse(data);
    }
    this.initializeForm();
    this.filteredOptions = this.options.slice();
  }

  ngOnInit(): void {
    this.initializeForm();
  }

  initializeForm() {
    this.addForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      username: new FormControl('', [Validators.required]),
      firstName: new FormControl('', [Validators.required]),
      lastName: new FormControl('', [Validators.required]),
      birthDate: new FormControl(new Date(), [Validators.required]),
      basicSalary: new FormControl(0, [Validators.required]),
      statusControl: new FormControl<Status | null>(null, [
        Validators.required,
      ]),
      groupControl: new FormControl('', [Validators.required]),
      description: new FormControl(new Date(), [Validators.required]),
    });
  }
  filter(): void {
    const filterValue = this.input.nativeElement.value.toLowerCase();
    this.filteredOptions = this.options.filter((o) =>
      o.toLowerCase().includes(filterValue)
    );
  }
  // ngOnInit(): void {
  // }

  myFilter = (d: Date | null): boolean => {
    const day = d || new Date();
    const today = new Date();
    return day <= today;
  };

  onSubmit() {
    const dataInput = this.addForm.value
    
    let body = {
      username: dataInput.username,
      firstName: dataInput.firstName,
      lastName: dataInput.lastName,
      email: dataInput.email,
      birthDate: new Date (dataInput.birthDate),
      basicSalary: dataInput.basicSalary,
      status: dataInput.statusControl.value,
      group: dataInput.groupControl,
      description: new Date (dataInput.description),
    }
    this.currentData.push(body)
    localStorage.setItem('employeesData', JSON.stringify(this.currentData))
    this.router.navigate(['/home']);
  }

  onCancel() {
    this.router.navigate(['/home']);
  }

  get addFormData() {
    return this.addForm.controls;
  }
}
