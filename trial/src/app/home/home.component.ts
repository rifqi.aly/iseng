import { AfterViewInit, Component, ViewChild, Inject } from '@angular/core';
import { MatPaginator, MatPaginatorModule } from '@angular/material/paginator';
import { MatSort, MatSortModule } from '@angular/material/sort';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import moment from 'moment';
import { MatButtonModule } from '@angular/material/button';
import { Router } from '@angular/router';
import { CommonModule, DOCUMENT } from '@angular/common';
import Swal from 'sweetalert2';

export interface Employee {
  username: string;
  firstName: string;
  lastName: string;
  email: string;
  birthDate: any;
  basicSalary: number;
  status: string;
  group: string;
  description: any;
}

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatIconModule,
    MatTooltipModule,
    MatButtonModule,
  ],
  templateUrl: './home.component.html',
  styleUrl: './home.component.css',
})
export class HomeComponent implements AfterViewInit {
  dataFilter: any;

  displayedColumns: string[] = [
    'username',
    'firstName',
    'lastName',
    'email',
    'birthDate',
    'basicSalary',
    'status',
    'group',
    'description',
    'action',
  ];
  dataSource: MatTableDataSource<Employee>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    @Inject(DOCUMENT) private document: Document,
    private router: Router
  ) {
    const localStorage = document.defaultView?.localStorage;

    let employees: any = [];
    if (localStorage) {
      this.dataFilter = localStorage.getItem('search');
      if (!localStorage.getItem('employeesData')) {
        employees = Array.from({ length: 100 }, (_, k) =>
          createNewEmployee(k + 1)
        );
        localStorage.setItem('employeesData', JSON.stringify(employees));
      } else {
        let data: any = localStorage.getItem('employeesData');
        employees = JSON.parse(data);
        this.dataSource = new MatTableDataSource(employees);
      }
      if (localStorage.getItem('search')) {
        this.applyFilter({ target: { value: this.dataFilter } });
      }
    }
    // Assign the data to the data source for the table to render
    this.dataSource = new MatTableDataSource(employees);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: any) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
    localStorage.setItem('search', filterValue);
  }

  handleButtonAdd() {
    this.router.navigate(['/add']);
  }

  handleView(row: object) {
    this.router.navigate(['/detail']);
    localStorage.setItem('data', JSON.stringify(row));
  }

  handleEdit() {
    Swal.fire({
      icon: "warning",
      title: "Edit Success",
      text: "You've successfully pressed the Edit Button!",
    });
    
  }

  handleDelete() {
    Swal.fire({
      icon: "error",
      title: "Delete Success",
      text: "You've successfully pressed the Edit Button!",
    });
    
  }
}

/** Builds and returns a new User. */

function createNewEmployee(id: number): Employee {
  return {
    username: 'user' + id.toString(),
    firstName: 'first' + id.toString(),
    lastName: 'last' + id.toString(),
    email: `first${id}.last${id}@mail.com`,
    birthDate: moment(new Date()).subtract(
      Math.round(Math.random() * 100),
      'days'
    ),
    basicSalary: id * 100000,
    status: 'status' + id.toString(),
    group: 'group' + id.toString(),
    description: moment(new Date()).subtract(id, 'days'),
  };
}
