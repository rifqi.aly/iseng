import { Component, OnInit, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatDividerModule } from '@angular/material/divider';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { Router } from '@angular/router';
import moment from 'moment';

@Component({
  selector: 'app-detail',
  standalone: true,
  imports: [
    MatButtonModule,
    MatIconModule,
    MatProgressBarModule,
    MatDividerModule,
    MatCardModule,
  ],
  templateUrl: './detail.component.html',
  styleUrl: './detail.component.css',
})
export class DetailComponent implements OnInit {
  data: any;
  employee: any;
  birthDate: string;
  desc: string;
  wage: string;

  constructor(
    private router: Router,
    @Inject(DOCUMENT) private document: Document
  ) {
    const localStorage = document.defaultView?.localStorage;
    if (localStorage) {
      this.data = localStorage.getItem('data');
      this.employee = JSON.parse(this.data);
      this.birthDate = moment(this.employee.birthDate).format('DD MMMM YYYY');
      this.desc = moment(this.employee.description).format('DD MMMM YYYY');
      this.wage = formatter.format(this.employee.basicSalary);
    }
  }

  ngOnInit(): void {
    // console.log(JSON.parse(this.employee));
  }
  returnHome() {
    this.router.navigate(['/home']);
  }
}

const formatter = new Intl.NumberFormat('id-ID', {
  style: 'currency',
  currency: 'IDR',

  // These options are needed to round to whole numbers if that's what you want.
  //minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
  //maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)
});
