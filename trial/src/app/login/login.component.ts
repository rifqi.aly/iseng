import { Component, OnInit, Inject } from '@angular/core';
import { NgIf, DOCUMENT } from '@angular/common';
import { Router } from '@angular/router';

import {
  FormControl,
  FormGroup,
  Validators,
  FormsModule,
  ReactiveFormsModule,
  // FormBuilder,
} from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    NgIf,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
  ],
  templateUrl: './login.component.html',
  styleUrl: './login.component.css',
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  constructor(
    @Inject(DOCUMENT) private document: Document,
    private router: Router
  ) {
    const localStorage = document.defaultView?.localStorage;
    if (localStorage) {
      localStorage.clear();
    }

    this.initializeForm();
  }

  get loginFormData() {
    return this.loginForm.controls;
  }

  onSubmit() {
    const loginData = this.loginForm.controls;
    const email = loginData.email.value;
    const password = loginData.password.value;

    if (email == 'a@mail' && password == '123') {
      this.router.navigate(['/home']);
    } else {
      loginData.email.setErrors({ incorrect: true });
      loginData.password.setErrors({ incorrect: true });
    }
  }

  initializeForm() {
    this.loginForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required]),
    });
  }

  ngOnInit(): void {
    this.initializeForm();
  }
}
